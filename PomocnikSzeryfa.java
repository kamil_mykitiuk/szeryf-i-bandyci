package dzikizachod;

import dzikizachod.strategie.StrategiaPomocnikaSzeryfa;
import dzikizachod.strategie.StrategiaPomocnikaSzeryfaDomyslna;

/**
 * Created by kamil on 13/05/2017.
 */
public class PomocnikSzeryfa extends Gracz {


    public PomocnikSzeryfa() {
        super(Profesja.POMOCNIK_SZERYFA, 3 + losuj.nextInt(2));
        setStrategia(new StrategiaPomocnikaSzeryfaDomyslna());
    }

    public PomocnikSzeryfa(StrategiaPomocnikaSzeryfa strategia) {
        super(Profesja.POMOCNIK_SZERYFA, 3 + losuj.nextInt(2));
        setStrategia(strategia);
    }
}
