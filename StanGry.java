package dzikizachod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Created by kamil on 15/05/2017.
 */
public class StanGry {

    private int liczbaTur = 1;
    private Gracz aktywnyGracz;
    private Gracz szeryf;
    private List<Gracz> żywiGracze;
    private List<Gracz> gracze;
    private Map<Gracz, Integer> bilansZabójstw;
    private Collection<Gracz> ktoAtakowałSzeryfa;

    public StanGry(List<Gracz> gracze) {
        this.gracze = gracze;
        this.żywiGracze = new ArrayList<>(gracze);

        this.bilansZabójstw = new HashMap<>();
        for (Gracz g : gracze) bilansZabójstw.put(g, 0);

        this.ktoAtakowałSzeryfa = new HashSet<>();
        this.szeryf = znajdźSzeryfa(gracze);
        this.aktywnyGracz = szeryf;
    }

    private Gracz znajdźSzeryfa(List<Gracz> gracze) {
        for (Gracz gracz : gracze) {
            if (gracz.getProfesja() == Profesja.SZERYF) {
                return gracz;
            }
        }
        return gracze.get(0);
    }

    public Gracz getAktywnyGracz() {
        return aktywnyGracz;
    }

    public Gracz getSzeryf() {
        return szeryf;
    }

    public boolean czyAtakowałSzeryfa(Gracz gracz) {
        return ktoAtakowałSzeryfa.contains(gracz);
    }

    public int getBilansGracza(Gracz g) {
        return bilansZabójstw.get(g);
    }

    public void zmienAktywnegoGracza() {
        aktywnyGracz = gracze.get((gracze.indexOf(aktywnyGracz) + 1) % gracze.size());
    }

    public void zmieńLiczbęTur() {
        liczbaTur++;
    }

    public int getLiczbaTur() {
        return liczbaTur;
    }

    public List<Gracz> getGraczePrzedSzeryfem() {
        int zasięg = getOdległośćDoSzeryfa() - 1;
        return spójrzWPrawo(zasięg);
    }


    public int getOdległośćDoSzeryfa() {
        return (żywiGracze.indexOf(szeryf) - żywiGracze.indexOf(aktywnyGracz) + żywiGracze.size()) % żywiGracze.size();
    }

    public List<Gracz> getPotencjalneCele() {
        int zasięg = aktywnyGracz.getZasięg();
        HashSet<Gracz> wynik = new HashSet<>();
        wynik.addAll(spójrzWPrawo(zasięg));
        wynik.addAll(spójrzWLewo(zasięg));
        return new ArrayList<>(wynik);
    }

    public Gracz lewySąsiad() {
        return spójrzWLewo(1).get(0);
    }

    public Gracz prawySąsiad() {
        return spójrzWPrawo(1).get(0);
    }

    private List<Gracz> spójrzWPrawo(int zasięg) {
        int modulo = żywiGracze.size();
        int index = (żywiGracze.indexOf(aktywnyGracz) + 1) % modulo;
        List<Gracz> wynik = new ArrayList<>();
        for (int i = index; wynik.size() != zasięg; i = (i + 1) % modulo) {
            if (żywiGracze.get(i).equals(aktywnyGracz)) break;
            wynik.add(żywiGracze.get(i));
        }
        return wynik;
    }


    private List<Gracz> spójrzWLewo(int zasięg) {
        int modulo = żywiGracze.size();
        int index = (żywiGracze.indexOf(aktywnyGracz) - 1 + modulo) % modulo;
        List<Gracz> wynik = new ArrayList<>();
        for (int i = index; wynik.size() != zasięg; i = (i - 1 + modulo) % modulo) {
            if (żywiGracze.get(i).equals(aktywnyGracz)) break;
            wynik.add(żywiGracze.get(i));
        }

        return wynik;

    }


    public void wykonajWybuchDynamitu() {
        aktywnyGracz.zadajObrażenia(Dynamit.getDynamit().getObrażenia());
        aktywnyGracz.setDynamit(null);
        if (aktywnyGracz.jestMartwy()) {
            żywiGracze.remove(aktywnyGracz);
        }
    }

    public void przekażDynamit() {
        Dynamit dynamit = Dynamit.getDynamit();
        aktywnyGracz.setDynamit(null);
        prawySąsiad().setDynamit(dynamit);
    }

    public void wykonajAkcje(Akcja akcja, Gracz cel) {
        switch (akcja) {
            case ULECZ:
                ulecz(cel);
                break;
            case STRZEL:
                strzelDo(cel);
                break;
            case ZASIEG_PLUS_JEDEN:
                aktywnyGracz.zwiększZasięg(1);
                break;
            case ZASIEG_PLUS_DWA:
                aktywnyGracz.zwiększZasięg(2);
                break;
            case DYNAMIT:
                przekażDynamit();
                break;
        }
    }


    private void ulecz(Gracz cel) {
        if (cel.jestRanny()) cel.ulecz(1);
    }

    private void strzelDo(Gracz cel) {
        if (cel.equals(szeryf)) {
            ktoAtakowałSzeryfa.add(aktywnyGracz);
        }
        cel.zadajObrażenia(1);
        if (cel.jestMartwy()) {
            żywiGracze.remove(cel);
            zaaktualicujZabójstwa(cel);
        }
    }

    private void zaaktualicujZabójstwa(Gracz cel) {
        int modyfikator = (cel.getProfesja().equals(Profesja.BANDYTA)) ? 1 : -1;
        bilansZabójstw.merge(aktywnyGracz, modyfikator, (integer, integer2) -> integer + integer2);
    }


    public boolean jestRemis() {
        int maksymalnaLiczbaTur = 42;
        return (liczbaTur > maksymalnaLiczbaTur);
    }

    public boolean szeryfWygrał() {
        return bandyciSąMartwi();
    }

    public boolean bandyciWygrali() {
        return szeryfJestMarwy();
    }


    public boolean jestKoniec() {
        return jestRemis() || bandyciWygrali() || szeryfWygrał();
    }

    private boolean szeryfJestMarwy() {
        return szeryf.jestMartwy();
    }

    private boolean bandyciSąMartwi() {
        for (Gracz gracz : żywiGracze) {
            if (gracz.getProfesja() == Profesja.BANDYTA) {
                return false;
            }
        }
        return true;
    }


}
