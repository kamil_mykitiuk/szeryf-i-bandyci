package dzikizachod;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by kamil on 13/05/2017.
 */
public class Gra {
    private StanGry stanGry;


    public void rozgrywka(Gracz[] gracze, PulaAkcji pulaAkcji) {
        rozgrywka(java.util.Arrays.asList(gracze), pulaAkcji);
    }

    public void rozgrywka(Collection<Gracz> zawodnicy, PulaAkcji pulaAkcji) {
        List<Gracz> gracze = usadźGraczy(zawodnicy);
        stanGry = new StanGry(gracze);

        Komunikator.startGry(gracze);

        while (!stanGry.jestKoniec()) {

            Komunikator.startTury(stanGry.getLiczbaTur());

            for (int i = 0; i < gracze.size(); i++) {

                Gracz gracz = stanGry.getAktywnyGracz();
                gracz.dobierzKarty(pulaAkcji);

                Komunikator.kolejGraczaStart(gracz);
                gracz.wykonajRuch(stanGry);
                pulaAkcji.odddaj(gracz.oddajKarty());
                Komunikator.kolejGraczaKoniec(gracz, gracze);

                stanGry.zmienAktywnegoGracza();

                if (stanGry.jestKoniec()) {
                    zakończ(gracze, pulaAkcji);
                    return;
                }

            }
            stanGry.zmieńLiczbęTur();
            if (stanGry.jestKoniec()) {
                zakończ(gracze, pulaAkcji);
                return;
            }
        }
    }


    private List<Gracz> usadźGraczy(Collection<Gracz> zawodnicy) {
        List<Gracz> gracze = new LinkedList<>(zawodnicy);
        Collections.shuffle(gracze);
        int i = 1;
        for (Gracz g : gracze) {
            g.setNumerKrzesła(i);
            i++;
        }
        return gracze;
    }

    private void zakończ(List<Gracz> gracze, PulaAkcji pulaAkcji) {
        if (stanGry.szeryfWygrał()) {
            Komunikator.printWygranaSzeryfa();

        } else if (stanGry.bandyciWygrali()) {
            Komunikator.printWygranaBandytów();
        } else if (stanGry.jestRemis()) {
            Komunikator.printRemis();
        }
        for (Gracz gracz : gracze) {
            gracz.reset();
            pulaAkcji.odddaj(gracz.oddajKarty());
        }
        pulaAkcji.reset();
    }


}
