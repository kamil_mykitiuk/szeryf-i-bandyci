package dzikizachod;

import dzikizachod.strategie.Strategia;
import javafx.util.Pair;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Random;

/**
 * Created by kamil on 13/05/2017.
 */
public abstract class Gracz {

    protected static Random losuj = new Random();
    private static int maxKart = 5;
    private int numerKrzesła;

    private int zdrowie;
    private int maxZdrowie;
    private int zasięg = 1;

    private Profesja profesja;

    private Dynamit dynamit;

    private Collection<Akcja> ręka;
    private Collection<Akcja> urzyte;

    private Strategia strategia;

    public Gracz(Profesja profesja, int zdrowie) {
        this.profesja = profesja;
        this.zdrowie = zdrowie;
        this.maxZdrowie = zdrowie;
        ręka = new LinkedList<>();
        urzyte = new LinkedList<>();
    }

    protected void setStrategia(Strategia strategia) {
        this.strategia = strategia;
    }


    public void wykonajRuch(StanGry stanGry) {
        if (jestMartwy()) return;
        obsłóżDynamit(stanGry);
        Komunikator.listaRuchów(this);

        for (int i = 0; i < maxKart; ++i) {
            if (stanGry.jestKoniec()) return;
            Pair<Akcja, Gracz> decyzja = strategia.zdecyduj(stanGry, ręka);
            if (decyzja == null) return;
            Komunikator.ruch(decyzja.getKey(), decyzja.getValue().getNumerKrzesła(), numerKrzesła);

            stanGry.wykonajAkcje(decyzja.getKey(), decyzja.getValue());
            ręka.remove(decyzja.getKey());
            urzyte.add(decyzja.getKey());
        }
        if (maDynamit()) stanGry.przekażDynamit();
    }

    private void obsłóżDynamit(StanGry stanGry) {
        if (maDynamit()) {
            Komunikator.dynamit(this, dynamit.czyWybuchł());
            if (dynamit.jestZdetonowany()) stanGry.wykonajWybuchDynamitu();

        }
    }

    public boolean maDynamit() {
        return dynamit != null;
    }


    public boolean jestMartwy() {
        return (zdrowie <= 0);
    }

    public int stanZdroiwa() {
        return zdrowie;
    }

    public void zadajObrażenia(int a) {
        zdrowie -= a;
        if (jestMartwy()) {
            urzyte.addAll(ręka);
            ręka.clear();
        }
    }

    public boolean jestRanny() {
        return zdrowie < maxZdrowie;
    }

    public void ulecz(int a) {
        zdrowie += a;
    }

    public int getNumerKrzesła() {
        return numerKrzesła;
    }

    public void setNumerKrzesła(int numerKrzesła) {
        this.numerKrzesła = numerKrzesła;
    }

    public Profesja getProfesja() {
        return profesja;
    }

    public Collection<Akcja> getRęka() {
        return ręka;
    }

    private boolean czyDobrać() {
        return ręka.size() < 5;
    }

    public void dobierzKarty(PulaAkcji pulaAkcji) {
        try {
            while (this.czyDobrać()) {
                ręka.add(pulaAkcji.dobierz());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public Collection<Akcja> oddajKarty() {
        Collection<Akcja> doOddania = new LinkedList<>(urzyte);
        urzyte.clear();
        return doOddania;
    }


    public int getZasięg() {
        return zasięg;
    }

    public void zwiększZasięg(int i) {
        zasięg += i;
    }

    public void setDynamit(Dynamit dynamit) {
        this.dynamit = dynamit;
    }

    public void reset() {
        zdrowie = maxZdrowie;
        urzyte = ręka;
        ręka.clear();
        dynamit = null;
        zasięg = 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gracz gracz = (Gracz) o;

        if (numerKrzesła != gracz.numerKrzesła) return false;
        if (zdrowie != gracz.zdrowie) return false;
        if (maxZdrowie != gracz.maxZdrowie) return false;
        if (zasięg != gracz.zasięg) return false;
        if (profesja != gracz.profesja) return false;
        return strategia.equals(gracz.strategia);
    }

    @Override
    public int hashCode() {
        int result = numerKrzesła;
        result = 31 * result + maxZdrowie;
        result = 31 * result + profesja.hashCode();
        result = 31 * result + strategia.hashCode();
        return result;
    }
}
