package dzikizachod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by kamil on 13/05/2017.
 */
public class PulaAkcji {

    private List<Akcja> talia;
    private List<Akcja> urzyte;
    private Akcja zwróconyDynamit;

    public PulaAkcji() {
        talia = new ArrayList<>();
        urzyte = new ArrayList<>();
    }

    public void odddaj(Collection<Akcja> akcje) {
        akcje = oddzielDynamit(akcje);
        urzyte.addAll(akcje);
    }

    private Collection<Akcja> oddzielDynamit(Collection<Akcja> akcje) {
        if (akcje.contains(Akcja.DYNAMIT)) {
            zwróconyDynamit = Akcja.DYNAMIT;
            akcje.remove(Akcja.DYNAMIT);
        }
        return akcje;
    }

    public void dodaj(Akcja akcja, int ilość) {
        for (int i = 0; i < ilość; ++i) {
            urzyte.add(akcja);
        }
        this.przetasuj();
    }

    public Akcja dobierz() throws Exception {
        if (talia.isEmpty()) przetasuj();
        if (talia.isEmpty()) throw new Exception("Pusta talia, nie można dobrać");
        return talia.remove(talia.size() - 1);

    }

    private void przetasuj() {
        talia.addAll(urzyte);
        Collections.shuffle(talia);
        urzyte.clear();
    }

    public void reset() {
        if (zwróconyDynamit == Akcja.DYNAMIT) {
            talia.add(zwróconyDynamit);
            zwróconyDynamit = null;
        }
        talia.addAll(urzyte);
    }
}
