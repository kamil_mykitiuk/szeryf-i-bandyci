package dzikizachod;

import dzikizachod.strategie.StrategiaSzeryfa;
import dzikizachod.strategie.StrategiaSzeryfaDomyslna;

/**
 * Created by kamil on 13/05/2017.
 */
public class Szeryf extends Gracz {
    private static int zdrowieSzeryfa = 5;

    public Szeryf() {
        super(Profesja.SZERYF, zdrowieSzeryfa);
        setStrategia(new StrategiaSzeryfaDomyslna());
    }

    public Szeryf(StrategiaSzeryfa strategia) {
        super(Profesja.SZERYF, zdrowieSzeryfa);
        setStrategia(strategia);
    }
}
