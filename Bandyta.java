package dzikizachod;

import dzikizachod.strategie.Strategia;
import dzikizachod.strategie.StrategiaBandytyDomyslna;

/**
 * Created by kamil on 13/05/2017.
 */
public class Bandyta extends Gracz {

    public Bandyta() {
        super(Profesja.BANDYTA, 3 + Gracz.losuj.nextInt(2));
        setStrategia(new StrategiaBandytyDomyslna());
    }

    public Bandyta(Strategia strategia) {
        super(Profesja.BANDYTA, 3 + Gracz.losuj.nextInt(2));
        setStrategia(strategia);
    }


}
