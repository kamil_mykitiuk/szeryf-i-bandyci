package dzikizachod.strategie;

import dzikizachod.Akcja;
import dzikizachod.Gracz;
import dzikizachod.StanGry;
import javafx.util.Pair;

import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Created by kamil on 13/05/2017.
 */
public abstract class Strategia {
    protected static Random losuj = new Random();


    public Pair<Akcja, Gracz> zdecyduj(StanGry stanGry, Collection<Akcja> ręka) {
        Pair<Akcja, Gracz> decyzja;
        decyzja = czyUleczyć(stanGry, ręka);
        if (decyzja != null) return decyzja;
        decyzja = czyZwiększyćZasięg(stanGry, ręka);
        if (decyzja != null) return decyzja;
        decyzja = czyStrzelić(stanGry, ręka);
        if (decyzja != null) return decyzja;
        decyzja = czyUżyćDynamitu(stanGry, ręka);
        return decyzja;
    }

    protected Pair<Akcja, Gracz> decyzja(Akcja a, Gracz cel) {
        return new Pair<>(a, cel);
    }

    protected abstract Pair<Akcja, Gracz> czyUżyćDynamitu(StanGry stanGry, Collection<Akcja> ręka);

    protected abstract Pair<Akcja, Gracz> czyStrzelić(StanGry stanGry, Collection<Akcja> ręka);

    protected Pair<Akcja, Gracz> czyZwiększyćZasięg(StanGry stanGry, Collection<Akcja> ręka) {
        if (ręka.contains(Akcja.ZASIEG_PLUS_DWA)) {
            return decyzja(Akcja.ZASIEG_PLUS_DWA, stanGry.getAktywnyGracz());

        } else if (ręka.contains(Akcja.ZASIEG_PLUS_JEDEN)) {
            return decyzja(Akcja.ZASIEG_PLUS_JEDEN, stanGry.getAktywnyGracz());
        }
        return null;
    }

    protected Pair<Akcja, Gracz> czyUleczyć(StanGry stanGry, Collection<Akcja> ręka) {
        if (ręka.contains(Akcja.ULECZ) && stanGry.getAktywnyGracz().jestRanny()) {
            return decyzja(Akcja.ULECZ, stanGry.getAktywnyGracz());

        }
        return null;
    }

    protected Gracz losujCel(List<Gracz> gracze) {
        return gracze.get(losuj.nextInt(gracze.size()));
    }

    protected Pair<Akcja, Gracz> losowaDecyzja(List<Gracz> listaCeli, Akcja akcja) {
        if (listaCeli.isEmpty()) return null;
        Gracz cel = losujCel(listaCeli);
        return decyzja(akcja, cel);
    }


}
