package dzikizachod.strategie;

import dzikizachod.Akcja;
import dzikizachod.Gracz;
import dzikizachod.StanGry;
import javafx.util.Pair;

import java.util.Collection;
import java.util.List;

/**
 * Created by kamil on 17/05/2017.
 */
public class StrategiaPomocnikaSzeryfaZliczajaca extends StrategiaPomocnikaSzeryfa {


    @Override
    protected Pair<Akcja, Gracz> czyStrzelić(StanGry stanGry, Collection<Akcja> ręka) {
        if (!ręka.contains(Akcja.STRZEL)) return null;

        List<Gracz> potencjalneCele = podejrzaneOsoby(stanGry);
        return losowaDecyzja(potencjalneCele, Akcja.STRZEL);
    }
}
