package dzikizachod.strategie;

import dzikizachod.Akcja;
import dzikizachod.Gracz;
import dzikizachod.StanGry;
import javafx.util.Pair;

import java.util.Collection;
import java.util.List;

/**
 * Created by kamil on 17/05/2017.
 */
abstract public class StrategiaPomocnikaSzeryfa extends Strategia {

    @Override
    protected Pair<Akcja, Gracz> czyUżyćDynamitu(StanGry stanGry, Collection<Akcja> ręka) {
        if (!ręka.contains(Akcja.DYNAMIT)) return null;
        if (stanGry.getOdległośćDoSzeryfa() < 4) return null;

        List<Gracz> graczePrzedSzeryfem = stanGry.getGraczePrzedSzeryfem();
        graczePrzedSzeryfem.removeIf(gracz -> stanGry.getBilansGracza(gracz) >= 0);
        if (3 * graczePrzedSzeryfem.size() > 2 * stanGry.getOdległośćDoSzeryfa()) {
            return decyzja(Akcja.DYNAMIT, stanGry.prawySąsiad());
        }
        return null;
    }

    @Override
    protected Pair<Akcja, Gracz> czyUleczyć(StanGry stanGry, Collection<Akcja> ręka) {
        if (!ręka.contains(Akcja.ULECZ)) return null;

        if ((stanGry.lewySąsiad().equals(stanGry.getSzeryf()) || stanGry.prawySąsiad().equals(stanGry.getSzeryf()))
                && (stanGry.getSzeryf().jestRanny())) {
            return decyzja(Akcja.ULECZ, stanGry.getSzeryf());
        }

        return super.czyUleczyć(stanGry, ręka);
    }

    protected List<Gracz> podejrzaneOsoby(StanGry stanGry) {
        List<Gracz> potencjalneCele = stanGry.getPotencjalneCele();
        potencjalneCele.removeIf(gracz -> stanGry.getBilansGracza(gracz) >= 0 && !stanGry.czyAtakowałSzeryfa(gracz));
        return potencjalneCele;
    }


}
