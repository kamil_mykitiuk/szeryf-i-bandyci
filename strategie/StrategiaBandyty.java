package dzikizachod.strategie;

import dzikizachod.Akcja;
import dzikizachod.Gracz;
import dzikizachod.StanGry;
import javafx.util.Pair;

import java.util.Collection;
import java.util.List;

/**
 * Created by kamil on 17/05/2017.
 */
public abstract class StrategiaBandyty extends Strategia {


    @Override
    protected Pair<Akcja, Gracz> czyStrzelić(StanGry stanGry, Collection<Akcja> ręka) {
        if (!ręka.contains(Akcja.STRZEL)) return null;
        List<Gracz> potencjalneCele = stanGry.getPotencjalneCele();
        if (potencjalneCele.contains(stanGry.getSzeryf())) {
            return decyzja(Akcja.STRZEL, stanGry.getSzeryf());

        }
        return null;
    }

    protected Pair<Akcja, Gracz> czyUżyćDynamitu(StanGry stanGry, Collection<Akcja> ręka) {
        if (stanGry.getOdległośćDoSzeryfa() < 4 && ręka.contains(Akcja.DYNAMIT)) {

            return decyzja(Akcja.DYNAMIT, stanGry.prawySąsiad());

        }
        return null;
    }


}
