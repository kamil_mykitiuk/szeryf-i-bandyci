package dzikizachod.strategie;

import dzikizachod.Akcja;
import dzikizachod.Gracz;
import dzikizachod.StanGry;
import javafx.util.Pair;

import java.util.Collection;
import java.util.List;

/**
 * Created by kamil on 17/05/2017.
 */
abstract public class StrategiaSzeryfa extends Strategia {
    @Override
    protected Pair<Akcja, Gracz> czyUżyćDynamitu(StanGry stanGry, Collection<Akcja> ręka) {
        return null;
    }


    protected List<Gracz> podejrzaneOsoby(StanGry stanGry) {
        List<Gracz> potencjalneCele = stanGry.getPotencjalneCele();
        potencjalneCele.removeIf(gracz -> stanGry.getBilansGracza(gracz) >= 0 && !stanGry.czyAtakowałSzeryfa(gracz));
        return potencjalneCele;
    }
}
