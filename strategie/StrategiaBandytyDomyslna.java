package dzikizachod.strategie;

import dzikizachod.Akcja;
import dzikizachod.Gracz;
import dzikizachod.Profesja;
import dzikizachod.StanGry;
import javafx.util.Pair;

import java.util.Collection;
import java.util.List;

/**
 * Created by kamil on 17/05/2017.
 */
public class StrategiaBandytyDomyslna extends StrategiaBandyty {


    @Override
    protected Pair<Akcja, Gracz> czyStrzelić(StanGry stanGry, Collection<Akcja> ręka) {
        Pair<Akcja, Gracz> decyzja = super.czyStrzelić(stanGry, ręka);
        if (decyzja != null) return decyzja;
        if (!ręka.contains(Akcja.STRZEL)) return null;

        List<Gracz> potencjalneCele = stanGry.getPotencjalneCele();
        potencjalneCele.removeIf(gracz -> gracz.getProfesja() == Profesja.BANDYTA);
        return losowaDecyzja(potencjalneCele, Akcja.STRZEL);

    }
}
