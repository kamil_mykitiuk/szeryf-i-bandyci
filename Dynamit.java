package dzikizachod;

import java.util.Random;

/**
 * Created by kamil on 15/05/2017.
 */
public class Dynamit {
    private static Dynamit instancja = new Dynamit();
    final private int obrażenia = 3;
    private Random losuj = new Random();
    private boolean zdetonowany = false;

    private Dynamit() {
    }

    public static Dynamit getDynamit() {
        return instancja;
    }

    public boolean czyWybuchł() {
        zdetonowany = losuj.nextInt(6) == 0;
        return zdetonowany;
    }

    public int getObrażenia() {
        return obrażenia;
    }

    public boolean jestZdetonowany() {
        return zdetonowany;
    }
}
