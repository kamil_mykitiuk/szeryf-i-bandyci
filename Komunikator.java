package dzikizachod;

import java.util.Collection;
import java.util.List;

/**
 * Created by kamil on 13/05/2017.
 */
public class Komunikator {
    static String BANDYTA = "Bandyta";
    static String SZERYF = "Szeryf";
    static String POMOCNIK_SZERYFA = "Pomocnik Szeryfa";
    private static String nagłówekKoniec = "**KONIEC";
    private static String nagłówekStart = "**START";
    private static String nagłówekTura = "**Tura";
    private static String remis = "  REMIS - OSIĄGNIĘTO LIMIT TUR";
    private static String wygranaSzeryfa = "  WYGRANA STRONA: szeryf i pomocnicy";
    private static String wygranaBadnytów = "  WYGRANA STRONA: bandyci";
    private static String dynamitWybuchł = "    Dynamit: WYBUCHŁ";
    private static String dynamitPrzechodzi = "    Dynamit: PRZECHODZI DALEJ";
    private static String nagłoweGracz = "GRACZ";
    private static String nagłówekMartwy = "  MARTWY";
    private static String nagłówekGracze = "  Gracze:";
    private static String nagłowekAkcje = "    Akcje:";
    private static String nagłówekRuchy = "    Ruchy:";
    private static String liczbaŻyć = "liczba żyć";
    private static String martwy = "X";

    static public void printRemis() {
        System.out.println(nagłówekKoniec);
        System.out.println(remis);
    }

    static public void printWygranaSzeryfa() {
        System.out.println(nagłówekKoniec);
        System.out.println(wygranaSzeryfa);
    }

    public static void printWygranaBandytów() {
        System.out.println(nagłówekKoniec);
        System.out.println(wygranaBadnytów);
    }

    static public void startGry(Collection<Gracz> gracze) {
        System.out.println(nagłówekStart);
        printGracze(gracze);
    }

    private static void printGracze(Collection<Gracz> gracze) {
        System.out.println(nagłówekGracze);
        for (Gracz gracz : gracze) {
            printGracz(gracz);
        }
        System.out.println("");
    }

    private static void printGracz(Gracz gracz) {
        String komunikat = gracz.jestMartwy() ? gracz.getNumerKrzesła() + ": " + martwy + " (" + gracz.getProfesja() + ")" :
                (gracz.getNumerKrzesła() + ": " + gracz.getProfesja() + " (" + liczbaŻyć + ": " + gracz.stanZdroiwa() + ")");
        System.out.println("    " + komunikat);
    }

    static public void startTury(int tura) {
        System.out.println(nagłówekTura + " " + tura);
    }

    static public void printTuraKoniec(Collection<Gracz> gracze) {
        printGracze(gracze);
    }

    static public void kolejGraczaStart(Gracz gracz) {
        String komunikat = nagłoweGracz + " " + gracz.getNumerKrzesła() + " (" + gracz.getProfesja().toString() + ")";
        System.out.println(komunikat);
        if (gracz.jestMartwy()) System.out.println(nagłówekMartwy);
        else {
            printAkcje(gracz);
        }
    }

    static public void listaRuchów(Gracz gracz) {
        System.out.println(nagłówekRuchy);
        if (gracz.jestMartwy()) {
            System.out.println(nagłówekMartwy);
        }

    }


    static public void ruch(Akcja a, int cel, int kto) {
        String komunikat = a.toString();
        switch (a) {
            case ULECZ:
                komunikat += ((cel != kto) ? " " + cel : "");
                break;
            case STRZEL:
                komunikat += " " + cel;
                break;
            case ZASIEG_PLUS_JEDEN:
                break;
            case ZASIEG_PLUS_DWA:
                break;
            case DYNAMIT:
                break;
        }
        System.out.println("      " + komunikat);
    }

    static public void dynamit(Gracz gracz, boolean czyWybuchł) {
        String komunikat = (czyWybuchł) ? dynamitWybuchł : dynamitPrzechodzi;
        System.out.println(komunikat);
    }

    static private void printAkcje(Gracz gracz) {
        String komunikat = nagłowekAkcje + " [";
        for (Akcja a : gracz.getRęka()) {
            komunikat += a.toString() + ", ";
        }
        komunikat = komunikat.substring(0, komunikat.length() - 2);
        komunikat += "]";
        System.out.println(komunikat);
    }

    public static void kolejGraczaKoniec(Gracz aktywnyGracz, List<Gracz> gracze) {
        System.out.println("");
        if (!aktywnyGracz.jestMartwy()) printGracze(gracze);
    }
}
