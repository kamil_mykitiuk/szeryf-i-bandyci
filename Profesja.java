package dzikizachod;

/**
 * Created by kamil on 16/05/2017.
 */
public enum Profesja {
    SZERYF(Komunikator.SZERYF),
    BANDYTA(Komunikator.BANDYTA),
    POMOCNIK_SZERYFA(Komunikator.POMOCNIK_SZERYFA),
    UKRYTY("");

    final private String nazwaKlasy;

    Profesja(String nazwaKlasy) {
        this.nazwaKlasy = nazwaKlasy;
    }

    @Override
    public String toString() {
        return nazwaKlasy;
    }


}
